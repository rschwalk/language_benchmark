defmodule MvFilteredImagesEx.Main do

  def main(args) do
    args |> parse_args |> run
  end

  defp run({_opt, []}) do
    IO.puts(:stderr, "Least one argument is required!")
    IO.puts(:stderr, "Usage:")
    IO.puts(:stderr, "\tmv_filtered_images_ex [-s min_size] <path to image dir>")
    :error
  end
  defp run({[], path}) do
    IO.puts("No minimum size defined, 1080px will be used!")
    run({[min_size: 1080], path})
  end
  defp run({opt, path}) do
    IO.puts("Searching in #{path}...")
    path
    |> Path.expand
    |> list_all
    |> filter_files(opt[:min_size])
    |> move_images
    :ok
  end

  defp list_all(path) do
    unless String.ends_with?(path, "/toSmall") do
      expand(File.ls(path), path)
    else
      []
    end
  end

  defp expand({:ok, files}, path) do
    files
    |> Enum.flat_map(&list_all("#{path}/#{&1}"))
  end
  defp expand({:error, _}, path) do
    [path]
  end

  defp filter_files(files, min_size) do
    files
    |> Stream.filter(&image?(&1))
    |> Stream.filter(&small_image?(&1, min_size))
  end

  defp image?(path) do
    path
    |> String.downcase
    |> String.ends_with?(["png", "jpg", "jpeg"])
  end

  defp small_image?(image, min_size) do
    case ExImageInfo.info(File.read!(image)) do
      {_, width, height, _} ->
        size = if width > height, do: width, else: height
        size < min_size
      _ ->
        false
      end
  end

  defp move_one(image) do
    path = (image |> Path.dirname()) <> "/toSmall"
    if ! File.exists?(path) do
      File.mkdir(path)
    end
    new_image = path <> "/" <> (image |> Path.basename())
    case File.rename(image, new_image) do
      :ok -> :ok
      {:error, reason} ->
        IO.puts(:stderr, "I can't move the file (#{reason})")
        :error
    end
  end

  defp move_images([]) do
    IO.puts("I have not find any small images!")
  end
  defp move_images(images) do
    images
    |> Enum.each(&move_one(&1))
    IO.puts("I moved some images")
  end

  defp parse_args(args) do
    {opt, pos_args, _} = OptionParser.parse(args,
      aliases: [s: :min_size],
      strict: [min_size: :integer]
    )
    {opt, pos_args}
  end
end
