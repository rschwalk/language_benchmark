# MvFilteredImagesEx

### Build
```bash
mix escript.build
```
### Benchmark with 244 images (50 to small and will be moved)
```bash
Executed in  194.00 millis    fish           external 
   usr time  171.88 millis  562.00 micros  171.32 millis 
   sys time   93.82 millis  122.00 micros   93.70 millis 
```
### Benchmark with 27GB images
```bash
Executed in  332.71 secs   fish           external 
   usr time   15.27 secs  570.00 micros   15.27 secs 
   sys time    9.31 secs  128.00 micros    9.31 secs
```
