# MvFilteredImages Rust
### Benchmark with 244 images (50 to small and will be moved)
```bash
Executed in    3.00 millis    fish           external 
   usr time    2.98 millis  119.00 micros    2.86 millis 
   sys time    0.06 millis   55.00 micros    0.00 millis 
```
### Benchmark with 27GB images
```bash
Executed in  199.77 secs   fish           external 
   usr time    0.49 secs  151.00 micros    0.49 secs 
   sys time    1.72 secs   75.00 micros    1.72 secs
```
