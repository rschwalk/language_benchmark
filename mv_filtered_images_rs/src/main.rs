use std::env;
use std::process;

use mv_filtered_images::Config;

fn main() {
    let config = Config::new(env::args(), 1080).unwrap_or_else(|err| {
        eprintln!("Problem parsing the arguments: {}", err);
        process::exit(1);
    });

    if let Err(e) = mv_filtered_images::run(config) {
        eprintln!("Application error: {}", e);
        process::exit(1);
    }

}
