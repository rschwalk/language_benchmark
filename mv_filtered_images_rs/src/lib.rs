use std::error::Error;
use std::{env, fs, path};

extern crate walkdir;
extern crate image;

use walkdir::{DirEntry, WalkDir};
use image::image_dimensions;

pub struct Config {
    pub path: String,
    pub min_size: u32,
}

impl Config {
    pub fn new(mut args: env::Args, min_size: u32) -> Result<Config, &'static str> {
        args.next();

        let path = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a path"),
        };

        Ok(Config { path, min_size })
    }
}

fn is_image(entry: &DirEntry) -> bool {
    return match entry.file_name().to_str() {
        Some(file_name) => {
            //let file_name = file_entry.to_lowercase();
            if file_name.ends_with(".jpg") ||
                file_name.ends_with(".jpeg") ||
                file_name.ends_with(".png") {
                true
            } else {
                false
            }
        },
        None => {
            false
        }
    }
}

fn is_to_small(path: &path::Path, min_size: u32) ->  bool {
    return match image_dimensions(path) {
        Err(why) => {
            println!("ERROR reading the image dimensions! {:?}", why);
            false
        },
        Ok(dimensions) => {
            let size = if dimensions.0 <= dimensions.1 {
                dimensions.1
            } else {
                dimensions.0
            };
            size < min_size
        },
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let mut i = 0;
    let dest_dir = path::Path::new(&config.path).join("toSmall");
    if !dest_dir.exists() {
        match fs::create_dir(&dest_dir) {
            Err(why) => println!("ERROR creating the folder! {:?}", why.kind()),
            Ok(_) => {}
        }
    }
    for entry in WalkDir::new(&config.path) {
        match entry {
            Err(why) => println!("ERROR reading the folder! {:?}", why),
            Ok(file) => {
                if is_image(&file) && is_to_small(file.path(), config.min_size) {
                    let new_file = dest_dir.join(file.file_name());
                    match fs::rename(file.path(), new_file) {
                        Err(why) => println!("ERROR moving the image ({:?})! {:?}",
                                                    file.path(),
                                                    why.kind()),
                        Ok(_) => i += 1,
                    }
                }
            }


        }
    }

    println!("I moved {} files!", i);

    Ok(())
}
