module mv_filtered_images.Lib

open System.IO
open System
open System.Drawing

let isImage (path : string) =
  match path with
  | path when path.EndsWith(".jpg", StringComparison.OrdinalIgnoreCase) -> true
  | path when path.EndsWith(".jpeg", StringComparison.OrdinalIgnoreCase) -> true
  | path when path.EndsWith(".png", StringComparison.OrdinalIgnoreCase) -> true
  | _ -> false

let isDirectory path =
  File.GetAttributes(path) = FileAttributes.Directory

let getImagesFrom directory =
  let entries = List.ofArray (Directory.GetFileSystemEntries(directory))
  printfn "I found [ %i ] entries in the directory [ %s ]" entries.Length directory

  let rec loopOnEntries allEntries allImages =

    match allEntries with
    | head::tail when isDirectory head ->
        printfn "Dir: %s" head
        let entriesOFSubDir = List.ofArray (Directory.GetFileSystemEntries(head))
        printfn "I found [ %i ] entries in the directory [ %s ]" entriesOFSubDir.Length head
        loopOnEntries (tail @ entriesOFSubDir) allImages

    | head::tail when isImage head ->
      loopOnEntries tail (allImages @ [head])

    | head::tail ->
        loopOnEntries tail allImages

    | [] -> allImages

  loopOnEntries entries []

let isToSmall maxSize path =
  let img = Image.FromFile(path)
  let imgSize = img.Size

  let size = if imgSize.Width > imgSize.Height then imgSize.Width else imgSize.Height

  size < maxSize


let run dir (maxSize:string) =
  let sizePars = Int32.TryParse(maxSize)
  match sizePars with
  | true,size ->
      getImagesFrom dir
      |> List.filter (fun path -> isToSmall size path)
      |> List.iter (fun img -> printfn "to smal: %A" img)
      0
  | _ ->
      eprintfn "ERROR: Size is not a valid number"
      1

