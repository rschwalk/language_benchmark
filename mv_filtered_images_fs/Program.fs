﻿module mv_filtered_images.Program

open System
open Lib

type Arguments = {
  directory: string option;
  maxSize: string;
  isSizeOptionExist: bool;
}

let parseCommandLine args =
  let defaultArguments = {
    directory = None;
    maxSize = "1080";
    isSizeOptionExist = false;
  }

  let rec parseCommandLineRec args optionsSoFar =
    match args with
    | [] -> optionsSoFar
    | "-s"::xs ->
      match xs with
      | size::xss ->
        let newOptionSoFar = { optionsSoFar with maxSize=size; isSizeOptionExist=true}
        parseCommandLineRec xss newOptionSoFar
      | _ ->
        eprintfn "Max Size needs a second argument"
        parseCommandLineRec xs optionsSoFar
    | dir::xs ->
      let newOptionSoFar = { optionsSoFar with directory=Some(dir)}
      parseCommandLineRec xs newOptionSoFar

  parseCommandLineRec args defaultArguments

let validateArguments args =
  match args with
  | { directory = None } ->
    eprintfn "MISSING ARGUMENT: The first argument schould be the directory!"
    false
  | { isSizeOptionExist = false } ->
    eprintfn "INFO: the default max size (1080) wil be used!"
    true
  | { directory = Some(_)} ->
    true

[<EntryPoint>]
let main argv =
  printfn "args: %A" argv

  let argList = argv |> Array.toList

  let arguments = parseCommandLine argList
  printfn "arguments: %A" arguments
  validateArguments arguments |> ignore

  Lib.run (Option.get arguments.directory) arguments.maxSize

