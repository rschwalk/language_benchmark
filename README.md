# language_benchmark

The same program written in different languages.

## Benchmark with 244 images (50 to small and will be moved)
### Rust
```bash
Executed in    3.00 millis    fish           external 
   usr time    2.98 millis  119.00 micros    2.86 millis 
   sys time    0.06 millis   55.00 micros    0.00 millis 
```
### Python
```bash
Executed in   73.21 millis    fish           external 
   usr time   61.20 millis  889.00 micros   60.31 millis 
   sys time   12.06 millis    0.00 micros   12.06 millis 
```
### Elixir
```bash
   Executed in  194.00 millis    fish           external 
      usr time  171.88 millis  562.00 micros  171.32 millis 
      sys time   93.82 millis  122.00 micros   93.70 millis
```

## Benchmark with 27GB images
### Python 
```bash
Executed in  184.26 secs   fish           external 
   usr time    5.93 secs  665.00 micros    5.93 secs 
   sys time    1.93 secs  150.00 micros    1.93 secs
```
### Rust
```bash
Executed in  199.77 secs   fish           external 
   usr time    0.49 secs  151.00 micros    0.49 secs 
   sys time    1.72 secs   75.00 micros    1.72 secs
```
### Elixir
```bash
Executed in  332.71 secs   fish           external 
   usr time   15.27 secs  570.00 micros   15.27 secs 
   sys time    9.31 secs  128.00 micros    9.31 secs
```
