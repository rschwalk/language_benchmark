import os, sys, shutil
from PIL import Image
from fnmatch import fnmatch

MAX_IMG_SIZE = 1080

def check_path(path):
    
    listOfFiles = os.listdir(path)

    targetDir = os.path.join(path, './toSmall')

    patterns = ['*.jpg', '*.png', '*.jpeg', '*.bmp']
    i = 0
    errorCount = 0
    for file in listOfFiles:
        try:
            if os.path.isdir(file):
                new_path = os.path.join(path, file)
                print('Check next path: {}'.format(new_path))
                check_path(new_path)
            elif any(fnmatch(file.lower(), patten) for patten in patterns):
                im = Image.open(file)
                width, height = im.size
                im.close()
                if width < height: 
                    ref = height
                else:
                    ref = width
                if ref < MAX_IMG_SIZE:
                    if not os.path.exists(targetDir):
                        os.makedirs(targetDir)
                    shutil.move(file, targetDir)
                    i += 1
        except:
            errorCount += 1
            continue

    print('I moved {} files from {}. ({} error(s) occured!) in {}'.format(i, len(listOfFiles), errorCount, path))

def check_recursive(path):
    patterns = ['*.jpg', '*.png', '*.jpeg', '*.bmp']

    for root, directories, filenames in os.walk(path):
        i = 0
        errorCount = 0
        for filename in filenames:
            try:
                if not root.endswith('toSmall'):
                    full_filename = os.path.join(root, filename)
                    if any(fnmatch(full_filename.lower(), patten) for patten in patterns):
                        im = Image.open(full_filename)
                        width, height = im.size
                        im.close()
                        if width < height: 
                            ref = height
                        else:
                            ref = width
                        if ref < MAX_IMG_SIZE:
                            target_dir = os.path.join(root, './toSmall')
                            if not os.path.exists(target_dir):
                                os.makedirs(target_dir)
                            shutil.move(full_filename, target_dir)
                            i += 1
            except Exception as e:
                print(e)
                errorCount += 1
                continue
        if not root.endswith('toSmall'):
            print('I moved {} files from {}. ({} error(s) occured!) in {}'.format(i, len(filenames), errorCount, root))


def main(path):
    os.chdir(path)
    # check_path(path)
    check_recursive(path)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        print('Wrong argument')