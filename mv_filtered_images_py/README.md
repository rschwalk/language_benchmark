# MvFilteredImagesPy

### Benchmark with 244 images (50 to small and will be moved)
```bash
Executed in   73.21 millis    fish           external 
   usr time   61.20 millis  889.00 micros   60.31 millis 
   sys time   12.06 millis    0.00 micros   12.06 millis 
```
### Benchmark with 27GB images
```bash
Executed in  184.26 secs   fish           external 
   usr time    5.93 secs  665.00 micros    5.93 secs 
   sys time    1.93 secs  150.00 micros    1.93 secs
```
